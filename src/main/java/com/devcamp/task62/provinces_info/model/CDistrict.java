package com.devcamp.task62.provinces_info.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

//import com.fasterxml.jackson.annotation.JsonIdentityInfo;
//import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "district")
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class CDistrict {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "name")
    private String name;
    @Column(name = "prefix")
    private String prefix;

    @ManyToOne
    @JoinColumn(name = "province_id")
    private CProvince province;
    
    @OneToMany(mappedBy = "district", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<CWard> ward;

    public CDistrict() {
    }

    public CDistrict(int districtId, String name, String prefix, CProvince province, Set<CWard> ward) {
        this.id = districtId;
        this.name = name;
        this.prefix = prefix;
        this.province = province;
        this.ward = ward;
    }

    public int getId() {
        return id;
    }

    public void setId(int districtId) {
        this.id = districtId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public CProvince getProvince() {
        return province;
    }

    public void setProvince(CProvince province) {
        this.province = province;
    }

    public Set<CWard> getWard() {
        return ward;
    }

    public void setWard(Set<CWard> ward) {
        this.ward = ward;
    }

    
}
