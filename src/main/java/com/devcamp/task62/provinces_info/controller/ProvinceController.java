package com.devcamp.task62.provinces_info.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task62.provinces_info.model.CDistrict;
import com.devcamp.task62.provinces_info.model.CProvince;
import com.devcamp.task62.provinces_info.repository.JProvinceRepository;
import com.devcamp.task62.provinces_info.service.ProvinceService;

@RequestMapping("/")
@CrossOrigin
@RestController
public class ProvinceController {
    
    @Autowired
    JProvinceRepository provinceRepository;
    @Autowired
    ProvinceService provinceService;

    @GetMapping("/all-provinces")
    public ResponseEntity <List<CProvince>> getAllProvincesByService() {
        try {
            return new ResponseEntity<>(provinceService.getProvinceList(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);    
        }
    }
    
    @GetMapping("/province5")
    public ResponseEntity<List<CProvince>> getFiveProvinces(
            @RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "5") String size){
        try {
            org.springframework.data.domain.Pageable pageWithFiveProvince = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<CProvince> list = new ArrayList<CProvince>();
            provinceRepository.findAll(pageWithFiveProvince).forEach(list::add);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            return null;
        }
    }

    /*@GetMapping("/province/{id}")
    public ResponseEntity<CProvince> getProvinceById(@PathVariable("id") Long id){
        Optional<CProvince> provinceData = provinceRepository.findById(id);
        if (provinceData.isPresent()){
            return new ResponseEntity<>(provinceData.get(), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }*/

    @GetMapping("/district_by_province_id")
    public ResponseEntity<Set<CDistrict>> getDistrictByProvinceId(@RequestParam(value = "id") int id){
        try {
            Set<CDistrict> listDistricts = provinceService.getDistrictByProvinceId(id);
            if (listDistricts != null){
                return new ResponseEntity<>(listDistricts, HttpStatus.OK);
            }else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
            
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
