package com.devcamp.task62.provinces_info.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task62.provinces_info.model.CDistrict;
import com.devcamp.task62.provinces_info.model.CProvince;
import com.devcamp.task62.provinces_info.repository.JProvinceRepository;


@Service
public class ProvinceService {
    
    @Autowired
    JProvinceRepository provinceRepository;
    public ArrayList<CProvince> getProvinceList(){
        
        ArrayList<CProvince> listProvinces = new ArrayList<>();
        provinceRepository.findAll().forEach(listProvinces::add);
        return listProvinces;
    }

    public Set<CDistrict> getDistrictByProvinceId(int id){
        CProvince targetCProvince = provinceRepository.findById(id);
        if (targetCProvince != null){
            return targetCProvince.getDistricts();
        }else{
            return null;
        }
    }

}
