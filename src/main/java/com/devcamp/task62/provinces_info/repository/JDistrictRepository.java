package com.devcamp.task62.provinces_info.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task62.provinces_info.model.CDistrict;

public interface JDistrictRepository extends JpaRepository<CDistrict, Integer>{
    CDistrict findById(int id);
}
