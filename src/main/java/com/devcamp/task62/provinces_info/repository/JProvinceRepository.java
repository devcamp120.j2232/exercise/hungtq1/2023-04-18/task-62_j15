package com.devcamp.task62.provinces_info.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task62.provinces_info.model.CProvince;

public interface JProvinceRepository extends JpaRepository<CProvince, Long>{

    CProvince findById(int id);

   
    
}
