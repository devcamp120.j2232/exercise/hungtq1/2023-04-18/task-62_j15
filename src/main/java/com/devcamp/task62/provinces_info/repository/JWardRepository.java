package com.devcamp.task62.provinces_info.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task62.provinces_info.model.CWard;

public interface JWardRepository extends JpaRepository<CWard, Integer> {
    
}
